<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function () {

    Route::get('login', function(){
        return response()->json([
            'data'=>'',
            'state'=>false,
            'msg'=>'You Must login First'
        ]);
    })->name('login');

    Route::post('/users/register', 'Api\AuthController@register');
    Route::post('/users/register/confirm', 'Api\AuthController@RegisterConfirmationCode');
    Route::post('/users/login', 'Api\AuthController@login');
    Route::post('/users/forgot/password', 'Api\AuthController@forgotpassword');
    Route::post('/users/forgot/confirmation', 'Api\AuthController@ConfirmationCode');
    Route::post('/users/forgot/change', 'Api\AuthController@ChangePassword');
    Route::get('/users/{id}', 'Api\UsersController@Show');
    Route::get('/courts/{id}', 'Api\CourtsController@Show');
    Route::get('/sports', 'Api\CourtsController@Sport');
    Route::get('/sports/{id}', 'Api\CourtsController@SportDetails');
    Route::post('/check/courts', 'Api\CourtsController@CheckTime');
    Route::get('/products/{id}', 'Api\CourtsController@Products');
    Route::get('/sport/{id}', 'Api\CourtsController@SportName');
    Route::post('/error', 'Api\AuthController@Error');
    Route::post('/user/update/token', 'Api\UsersController@UpdateToken');
    Route::post('/user/sendNotifcation', 'Api\UsersController@SendNotifcation');
    Route::post('/user/all/sendNotifcation', 'Api\UsersController@SendNotifcationAll');
    Route::post('/checkAvailability', 'Api\CourtsController@CheckAvailability');
    Route::get('/pay', 'Api\PaymentController@pay');
    Route::get('/mobilePaymentSuccess', 'Api\PaymentController@mobilePaymentSuccess');
    Route::get('/mobilePaymentFail', 'Api\PaymentController@pay');
    Route::post('/check/favourit', 'Api\CourtsController@CheckFavourit');
    Route::get('/user/book/file/{id}', 'Api\BookingController@BookFile');
    

    Route::group(['middleware' => ['auth:api']], function(){
        Route::get('/user', 'Api\UsersController@User');
        Route::get('/users', 'Api\UsersController@Users');
        Route::post('/users/edite', 'Api\UsersController@EditeUser');
        Route::get('/user/like', 'Api\UsersController@UsersLiked');
        Route::post('/user/add/like', 'Api\UsersController@UsersAddFavourite');
        Route::post('/user/add/review', 'Api\UsersController@UsersAddReview');
        Route::get('/user/review', 'Api\UsersController@UsersReview');
        Route::post('/book/add', 'Api\BookingController@AdBook');
        Route::get('/user/book/up', 'Api\BookingController@UsersBookUp');
        Route::get('/user/book/past', 'Api\BookingController@UsersBookPast');
        Route::get('/user/book/{id}', 'Api\BookingController@Show');
        Route::post('/user/book/email', 'Api\BookingController@BookEmail');

    });



});
