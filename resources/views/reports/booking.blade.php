@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop


@section('page_header')
    <h1 class="page-title"><i class="voyager-bar-chart"></i>Daily Sales Report</h1>
    @include('voyager::multilingual.language-selector')
@stop
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
@section('content')

<div id="dvData">
<table>
    <thead>
        <tr>
            <th>Customer Name </th>
            <th>Customer Number </th>
            <th>Sport</th>
            <th>court name  </th>
            <th>Time slot from</th>
            <th>Duration</th>
            <th>Date of booking </th>
            <th>Invoice amount before discount,VAT</th>
            <th>Discount amount</th>
            <th>Products</th>
            <th>VAT amount</th>
            <th>All Total</th>
            <th>Paid Amount </th>
            <th>Balance</th>
            <th>Invoice date </th>
            <th>Method of payment</th>                                                                                                           
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
        <tr>
            <td>{{$item->user->name}}</td>
            <td>{{$item->user->contacts}}</td>
            <td>FootBall</td>
            <td>{{$item->pitch}}</td>
            <td>{{$item->from}}</td>
            <?php 
            $t1 = strtotime($item->from);
            $t2 = strtotime($item->to);
            $duration = gmdate('H:i', $t2 - $t1);
            ?>            
            <td>{{$duration}}</td>
            <td>{{$item->created_at}}</td>
            <td>{{$item->price}}</td>
            <td><?=$dicsount = $item->dicsount * $item->total / 100; ?></td>
            <?php $product = DB::table('order_products')->where('book_id',$item->id)->value('total'); ?>
            <td>{{$product}}</td>  
            <td><?=$va = ($item->total + $product - $dicsount) * setting('admin.vat') / 100?></td>
            <?php $ftotl = $product + $va + $item->total?>          
            <td><?=$t = $ftotl - $dicsount?></td>
            <?php $paid =  DB::table('payment')->where('book_id',$item->id)->sum('amount') ?>
            <td>{{$paid}}</td>
            <td>{{$t - $paid}}</td>
            <td></td>
            <td>{{$item->method}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>


<input type="button" id="btnExport" class="btn btn-success" value=" Export Report To Excel " />

<style>
    table{
        background: #fff;
        margin: 0 0 1.5em;
        color: #76838f;
        border-collapse: collapse;
        border-spacing: 0;
        width: 100%;

    }
    th {
        border-color: #eaeaea;
        background: #f8fafc;
    }
    tr {
        line-height: 2.5;
    }        
    table>thead>tr>th {
        border-bottom: 1px solid #e4eaec;
        border-right: 1px solid #e4eaec;
        text-align: center;
    }   
    td {
        text-align: center;
        border-right: 1px solid #e4eaec;
        border-bottom: 1px solid #e4eaec;
    }
    table>tfoot>tr>th, table>thead>tr>th {
        font-weight: 400;
        color: #526069;
    }     
    tr.sum {
        background: #ddd;
    }      
</style>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

<script>
$(document).ready(function() {

    table = $('#dataTable').DataTable( {
        dom: 'Bfrtip',
        destroy: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );

    $("#btnExport").click(function (e) {
        window.open('data:application/vnd.ms-excel,' + $('#dvData').html());
        e.preventDefault();
    });

});
</script>


@endsection