@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop


@section('page_header')
    <h1 class="page-title"> <i class="voyager-bar-chart"></i> Academy Report</h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="select">
        <form action="" method="get">
            <div class="col-sm-12" style="margin-bottom: 20px;">
                <div class="col-sm-3">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Subscriber</label>
                    <div class="col-sm-10">
                        <?php $users = DB::table('users')
                        ->where('type', 2)
                        ->get(); ?>
                        <select class="form-control input-sm" name="users">
                            <option value="">Select Subscriber</option>
                            @foreach ($users as $task)
                                <option value="{{ $task->id }}" @if (isset($_GET['users']) && $_GET['users'] > 0) 
                                    @if ($task->id==$_GET['users']) selected @endif
                            @endif>{{ $task->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Court</label>
                    <div class="col-sm-10">
                        <?php $courts = DB::table('courts')->get(); ?>
                        <select class="form-control input-sm" name="court">
                            <option value="">Select Court</option>
                            @foreach ($courts as $task)
                                <option value="{{ $task->id }}" @if (isset($_GET['court']) && $_GET['court'] > 0) 
                                    @if ($task->id==$_GET['court']) selected @endif
                            @endif>{{ $task->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Sport</label>
                    <div class="col-sm-10">
                        <?php $courts = DB::table('sports')->get(); ?>
                        <select class="form-control input-sm" name="sport">
                            <option value="">Select Sport</option>
                            @foreach ($courts as $task)
                                <option value="{{ $task->id }}" @if (isset($_GET['sport']) && $_GET['sport'] > 0) 
                                    @if ($task->id==$_GET['sport']) selected @endif
                            @endif>{{ $task->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">

                <div class="col-sm-3">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Time</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="from" @if (isset($_GET['from']) && $_GET['from'] > 0) value="{{ $_GET['from'] }}" @endif>
                    </div>
                </div>

                <div class="col-sm-3">
                    <label for="staticEmail" class="col-sm-2 col-form-label">To</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="to" @if (isset($_GET['to']) && $_GET['to'] > 0) value="{{ $_GET['to'] }}" @endif>
                    </div>
                </div>

                <div class="col-sm-3">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Academy</label>
                    <div class="col-sm-10">
                        <?php $users = DB::table('academies')->get(); ?>
                        <select class="form-control input-sm" name="academies">
                            <option value="">Select Academy</option>
                            @foreach ($users as $task)
                                <option value="{{ $task->id }}" @if (isset($_GET['academies']) && $_GET['academies'] > 0)  @if ($task->id==$_GET['academies']) selected @endif
                            @endif>{{ $task->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>

            </div>

        </form>
    </div>


    <div id="dvData">
        <table>
            <thead>
                <tr>
                    <th>Sport</th>
                    <th>Court name </th>
                    <th>Time from</th>
                    <th>Time to</th>
                    <th>Academy Name </th>
                    <th>Subscription Fees </th>
                    <th>Subscriber </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $item)
                    <tr>
                        <?php
                        $sport = DB::table('sports')
                        ->where('id', $item->academies->sport)
                        ->value('name');
                        $court = DB::table('courts')
                        ->where('id', $item->academies->court)
                        ->value('name');
                        ?>

                        <td>{{ $sport }}</td>
                        <td>{{ $court }}</td>
                        <td>{{ $item->start_date }}</td>
                        <td>{{ $item->enf_date }}</td>
                        <td>{{ $item->academies->name }}</td>
                        <td>{{ $item->subscription_fees }}</td>
                        <td>{{ $item->user->name }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>


    <input type="button" id="btnExport" class="btn btn-success" value=" Export Report To Excel " />

    <style>
        .select {
            margin-bottom: 30px;
            overflow: hidden;
        }

        table {
            background: #fff;
            margin: 0 0 1.5em;
            color: #76838f;
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;

        }

        th {
            border-color: #eaeaea;
            background: #f8fafc;
        }

        tr {
            line-height: 2.5;
        }

        table>thead>tr>th {
            border-bottom: 1px solid #e4eaec;
            border-right: 1px solid #e4eaec;
            text-align: center;
        }

        td {
            text-align: center;
            border-right: 1px solid #e4eaec;
            border-bottom: 1px solid #e4eaec;
        }

        table>tfoot>tr>th,
        table>thead>tr>th {
            font-weight: 400;
            color: #526069;
        }

        tr.sum {
            background: #ddd;
        }

    </style>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>

    <script>
        $(document).ready(function() {

            table = $('#dataTable').DataTable({
                dom: 'Bfrtip',
                destroy: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });

            $("#btnExport").click(function(e) {
                window.open('data:application/vnd.ms-excel,' + $('#dvData').html());
                e.preventDefault();
            });

        });

    </script>

@endsection
