@extends('layouts.user')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Add -on</h1>
    </div>
   
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Products</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                {!! Form::open(['route' => 'client.check.courts', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data"]) !!}
                <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Sno</th>
                            <th>Prodct Name</th>
                            <th>Rate</th>
                            <th>Quantity</th>
                            
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($products as $key=> $product)
                        <tr>
                           <td>
                               {{$product->id}} 
                            </td>
                            <td>{{$product->title}}
                            
                            <td>
                               {{$product->price}} 
                            </td>

                             <td>
                               <input type="number" name="qty" class="form-control">
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                    
                </table>
                <button type="submit" class="btn btn-primary">Proceed</button>
                
                {!! Form::close() !!}
            </div>
        </div>
    </div>
   
</div>
<!-- /.container-fluid -->
@endsection