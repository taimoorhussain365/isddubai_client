@extends('layouts.user')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Thank You!</h1>
        <h3>Review your payment</h3>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <!-- Default Card Example -->
            <div class="card mb-4">
                <div class="card-header">
                    <h6 class="m-0 font-weight-bold text-primary">Booking Details</h6>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="dataTable1" width="100%" cellspacing="0">
                        <thead>
                            <th>Booking #</th>
                            <th>Sport</th>
                            <th>Pitch</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Products*Qty</th>
                            <th>Rate</th>
                            <th>Total</th>
                        </thead>
                        <tbody>
                           
                                @foreach($details as $key=> $detail)

                                <tr>
                                    <td>{{$pitch = $detail['booking_id']}}</td>
                                <td>
                                    {{$sports = $detail['sports']}}<br>
                                </td>
                                <td>
                                    {{$pitch = $detail['pitch']}}<br>
                                </td>
                                <td>
                                    {{$detail['from']}}<br>
                                </td>
                                <td>
                                    {{$detail['to']}}
                                </td>
                                <td>
                                @if(count($detail['name']) !==0)
                                   @foreach($detail['name'] as $key=> $productName)
                                   {{$productName}}
                                   <br>
                                   @endforeach
                                @endif
                                </td>
                                <td>
                                   
                                  CourtRate: {{$detail['court_rate']}}<br>
                                  Product Sum: {{$detail['product_total']}}<br>
                                   
                                </td>
                                 <td>
                                   
                                  {{$totalAmount = $detail['total']}}<br>

                                </td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                    
                </div>
            </div>
            
        </div>
    </div>
    </div>
<!-- /.container-fluid -->
@endsection