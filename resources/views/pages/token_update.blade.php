@extends('layouts.user')
@section('content')
<!-- Begin Page Content -->

<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">

        <div class="mb-40">
            <h2 class="maintitle">REVIEW BOOKING</h2>

            <p class="maindesc --big">
                Please review your selection and confirm by clicking on the button to proceed to payment
            </p>
        </div>

        <div class="row">
            <div class="col-lg-3">
                @include('includes.sidebar-navigation')
            </div>
                    <?php
                    
                        $randomnum=rand();
                        
                        $returnurl= URL::current();
                        // echo $returnurl;
                        $returnurl2= URL::to('/');
                        
                        $shaString  = '';
                        
                        $arrData    = array(
                        'service_command'           =>  'AUTHORIZATION',
                        'access_code'               =>  'GktW1EwizGB7tZ51ocEG',
                        'merchant_identifier'       =>  'LmIAIBAK',
                        'merchant_reference'        =>  $randomnum,
                        'language'                  =>  'en',
                        'token_name'                =>  '4246e3d9e97844e4aa148414ce1c2539',
                        'card_holder_name'          =>  'taimoor',
                        'currency'                  =>  'AED',
                        'token_status'              =>  'Active',
                        'new_token_name'            =>  'Test1',
                        );
                        
                        ksort($arrData);
                        foreach ($arrData as $key => $value) {
                            $shaString .= "$key=$value";
                        }
                        
                        $shaString = "83/cPmhQ/ub0O3i8z0Qp4j-[" . $shaString . "83/cPmhQ/ub0O3i8z0Qp4j-[";
                        $signature = hash("sha256", $shaString);
                        
                        $requestParams = array(
                        'service_command'           =>  'AUTHORIZATION',
                        'access_code'               =>  'GktW1EwizGB7tZ51ocEG',
                        'merchant_identifier'       =>  'LmIAIBAK',
                        'merchant_reference'        =>  $randomnum,
                        'language'                  =>  'en',
                        'token_name'                =>  '4246e3d9e97844e4aa148414ce1c2539',
                        'signature'                 =>  $signature,
                        'card_holder_name'          =>  'taimoor',
                        'currency'                  =>  'AED',
                        'token_status'              =>  'Active',
                        'new_token_name'            =>  'Test1',
                        );
                        
                      
                        // $redirectUrl = 'https://paymentservices.payfort.com/FortAPI/paymentApi';
                        $redirectUrl = 'https://sbpaymentservices.payfort.com/FortAPI/paymentApi';

                        echo "<form action='$redirectUrl' method='post' name='frm' class='default-form'> \n";
                        
                        foreach ($requestParams as $a => $b) {

                        echo "\t<input type='hidden' name='".htmlentities($a)."' value='".htmlentities($b)."'>\n";
                        }
                        echo "\t<button type='submit' class='btn --btn-primary'>submit</button>\n";
                        echo "\t</form>\n";
                    ?>
                    {{-- <button type="submit" class="btn btn-primary" >submit</button> --}}

                </div>
            </div>
        </div>

    </div>
</section>
    
     
<!-- /.container-fluid -->
@endsection