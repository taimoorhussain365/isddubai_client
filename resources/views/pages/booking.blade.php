@extends('layouts.user')
@section('content')
<!-- Begin Page Content -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    {{-- <link rel="stylesheet" href="https://frontendscript.com/demo/jquery-timepicker-24-hour-format/dist/wickedpicker.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet"/> --}}

    {{-- <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script> --}}

<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">

        <div class="mb-40">
            <h2 class="maintitle">Book your session at ISD!</h2>

            <p class="maindesc --big">
                Please choose your sport, pitch or court, booking day, time & duration, from the drop-down menu.
                <br>
                If you have any questions, please call us on 04 448 1555 and we will more than happy to help you.
            </p>
        </div>

        <div class="row">
            <div class="col-lg-3">
                @include('includes.sidebar-navigation')
            </div>

            <div class="col-lg-9">
                <div class="article right-side">

                    {!! Form::open(['route' => 'client.check.courts', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data", 'class'=>'default-form']) !!}
    
                        <!-- Default Card Example -->
                            
                        @if(!empty($error_status) && $error_status==1)
                        <div class="alert --danger" role="alert">
                            <strong>Oops!</strong> No Courts is Found
                        </div>
                        @endif
                        

                        <div class="row">
                            <div class="col-md-6">
                                <div class="control-group">
                                    {!! Form::label('sport', 'Select Sport', ['class' => 'form-label']) !!}
                                    {{ Form::select('sport',$sports, $request->sport ?? null, ['class' => 'form-field','placeholder'=>"Select Option",'required'=>'required']) }}
                                    @if ($errors->has('sport'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('sport') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="control-group">
                                    
                                    {!! Form::label('time', 'Time From', ['class' => 'form-label']) !!}
                                    <div class='input-group date' id='datetimepicker'>
                                    {{ Form::text('time',$request->time ?? null, ['class' => 'form-field','id'=>'time','autocomplete'=>"off",'required'=>'required']) }}
                                        <!--<input type='text' class="form-field" id="time" name="time" />-->
                                        <span class="input-group-addon">
                                          <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                    
                                    <!--{!! Form::label('time', 'Time From', ['class' => 'form-label']) !!}-->
                                    <!--{{ Form::time('time',$request->time ?? null, ['class' => 'form-field','id'=>'time','autocomplete'=>"off",'required'=>'required']) }}-->
                                    <!--@error('time')-->
                                    <!--<span class="invalid-feedback" role="alert">-->
                                    <!--    <strong>{{ $message }}</strong>-->
                                    <!--</span>-->
                                    <!--@enderror-->
                                </div>

                                @if(count($data)!=0)
                                <div class="control-group">
                                    {!! Form::label('pitch', 'Select Pitch', ['class' => 'form-label']) !!}
                                    {{ Form::select('pitch',$data,$request->pitch ?? null, ['class' => 'form-field']) }}
                                    @if ($errors->has('pitch'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('pitch') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                @endif
                            </div>

                            <div class="col-md-6">
                                <div class="control-group">
                                    <label for="date" class="form-label">{{ __('Date') }}</label>
                                    {{ Form::text('date',$request->date ?? null, ['class' => 'form-field','id'=>'date','autocomplete'=>"off",'required'=>'required']) }}
                                    @error('date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="control-group">
                                    {!! Form::label('Duration', 'Duration', ['class' => 'form-label']) !!}
                                    {{ Form::select('duration',$durations,$request->duration ?? null, ['class' => 'form-field', 'placeholder'=>"Select Duration",'required'=>'required', 'id' => 'duration']) }}
                                    @if ($errors->has('duration'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('duration') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        
                        <div class="control-group">
                            <button type="submit" class="btn --btn-primary" name="submit" value="1">Search Courts</button>
                            @if(count($data)!=0)
                            <button type="submit" class="btn --football fc-white" name="submit" value="2"
                            id="proceed">Next</button>
                            @endif
                        </div>

                    {!! Form::close() !!}
                        
                    <form method="post" action="{{route('client.reset.form')}}" id="hiddenForm" enctype="multipart/form-data" hidden>

                        <input type="hidden" value="{{$request->sport ?? null}}" name="sport">
                        <input type="hidden" value="{{$request->time ?? null}}" name="time">
                        <input type="hidden" value="{{$request->pitch ?? null}}" name="pitch">
                        <input type="hidden" value="{{$request->date ?? null}}" name="date">
                        <input type="hidden" value="{{$request->duration ?? null}}" name="duration">
                        
                        <input type="submit" value="submit" name="submit" id="hidden-btn">
                    </form>
                        
                    <div class="sport-detail">
                        <div class="football-images hidden">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Indoor Artificial</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/football/indoor-artificial.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/football/indoor-artificial.jpg')}}' alt='football-sport' class='img-fluid'>
                                    </a>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Outdoor Artificial</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/football/outdoor-artificial.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/football/outdoor-artificial.jpg')}}' alt='football-sport' class='img-fluid'>
                                    </a>
                                </div>

                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Outdoor Grass</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/football/grass-pitch.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/football/grass-pitch.jpg')}}' alt='football-sport' class='img-fluid'>
                                    </a>
                                </div>
                                
                            </div>
                        </div>
                        <div class="tennis-images hidden">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Tennis Court 1</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/tennis/tennis-court1.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/tennis/tennis-court1.jpg')}}' alt='tennis-sport' class='img-fluid'>
                                    </a>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Tennis Court 2</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/tennis/tennis-court2.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/tennis/tennis-court2.jpg')}}' alt='tennis-sport' class='img-fluid'>
                                    </a>
                                </div>

                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Tennis Court 3</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/tennis/tennis-court3.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/tennis/tennis-court3.jpg')}}' alt='tennis-sport' class='img-fluid'>
                                    </a>
                                </div>
                                
                            </div>
                        </div>
                        <div class="athletics-images hidden">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="bg-primary">
                                        <h5 class="fc-white ml-4 lh-large">Athletics Track</h5>
                                    </div>
                                    <a href="https://isddubai.com/assets-web/images/gallery/athletics/athletics-track.jpg" data-fancybox>
                                        <img src='{{ asset('https://isddubai.com/assets-web/images/gallery/athletics/athletics-track.jpg')}}' alt='athletics-sport' class='img-fluid'>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</section>    

<script>

$(document).ready(function(){
    
    $("#sport").change(function(){
        if (window.location.href.indexOf("client/check/courts") > -1) {
            $('#hidden-btn').click();
            return false;
        }
        sportImages();
    })
    
    /*On Load set sport images*/    
    sportImages();

    function sportImages(){
        let sport_value = $("#sport").val();
        
        if(typeof sport_value !== 'undefined'){
            if(sport_value === ""){
                //$(".sport-detail").html("");
            }
            else if(sport_value == "1"){
                $(".sport-detail div").removeClass('show');
                $(".sport-detail .football-images").addClass('show');
            }
            else if(sport_value == "2"){
                $(".sport-detail div").removeClass('show');
                $(".sport-detail .tennis-images").addClass('show');
            }
            else if(sport_value == "5"){
                $(".sport-detail div").removeClass('show');
                $(".sport-detail .athletics-images").addClass('show');
            }
            else{
                //$(".sport-detail").html("");
            }   
        }    
    }

//         var time = {!! str_replace("'", "\'", json_encode($request->time ?? '')) !!};
//         $("#time").val(time);   
        
//         if($("#time").val() == "23:00"){
            
//             $("#duration option").val(59);
            
//         }
//         else if($("#time").val() == "22:00"){
            
//             $("#duration option").val(59);
            
//         }else{
            
//             $("#duration option:nth-child(2)").val(60);
//             $("#duration option:nth-child(3)").val(90);
//             $("#duration option:nth-child(4)").val(120);            

//         }
        
//     })

    var dateToday = new Date();
    $( "#date" ).datepicker({
        
        minDate: 0,
 

    
    });
    
//     $("#time").focusout(function(){
//         var my_time = $(this).val();
        
//         if(my_time == "23:00"){
            
//             $("#duration option").val(59);
//         }
//         else if(my_time == "22:00"){
            
//             $("#duration option").val(59);
//         }
//         else{
//             $("#duration option:nth-child(2)").val(60);
//             $("#duration option:nth-child(3)").val(90);
//             $("#duration option:nth-child(4)").val(120);            
//         }
    })

</script>

    <!-- /.container-fluid -->
   

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script> --}}

<script type="text/javascript">
        
// var $i = jQuery.noConflict();
// $i(function () {
    
//     $i('#datetimepicker').datetimepicker({
//         format: 'HH:mm',
//         // disabledTimeIntervals: [[moment({ h: 0 }), moment({ h: 6 })], [moment({ h: 17, m: 30 }), moment({ h: 12 })]],
//         enabledHours: [0,1,2,3,4,5,6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,18,19,20,21,22,23],
//         stepping: 15
        
//     });
// });

$(function() {
    $('#time').timepicker({
        timeFormat: 'h:mm p',
        interval: 30,
        minTime: '05:30am',
        maxTime: '11:30pm',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
});

</script>

@endsection