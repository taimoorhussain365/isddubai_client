<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
    <style>
        @font-face {
            font-family: Druk;

        }

        body {
            font-family: 'Roboto', sans-serif;
            padding: 0px 30px;
        }

        h1,
        h4 {
            font-family: 'Roboto';
        }

        img {
            margin: 35px auto;
            display: block;
        }

        h4 {
            color: red;
            font-size: 23px;
            margin-bottom: 0;
        }

        li {
            list-style-type: none;
            font-size: 21px;
            font-weight: bolder;
            color: #3E2B64;
            line-height: 1.5;
        }

        ul {
            padding: 0;
        }

        footer {
            text-align: center;
            font-weight: bold;
        }

        p {
            margin-top: 0;
            margin-bottom: 5px;
        }

        span {
            margin-left: 10px;
        }

    </style>
</head>

<body>
    <div>
        <img src="https://bookings.isddubai.com/public/logo2.png" alt="" srcset="" width="150">
        <h1 style="text-align: center;color: #ffb938;font-size: 48px;letter-spacing: .8px;">TAX INVOICE</h1>
        <h4>CUSRTOMER DETAILS</h4>
        <ul>
            <?php $user = App\User::find($details['client_id']);
            $Booking = App\Booking::find($details['id']); ?>
            <li>Customer Name: <span>{{ $user->name }}</span></li>
            <li>Customer Email Address:<span>{{ $user->email }}</span> </li>
            <li>Customer Mobile No:: <span>{{ $user->phone }}</span></li>
            <li>Invoice Number : <span>{{ $Booking['code'] }}</span></li>
        </ul>

        <h4>BOOKING DETAILS</h4>
        <ul>
            <li>Sport: <span>{{ $Booking['sports'] }}</span></li>
            <li>Pitch/Court: <span>{{ $Booking['pitch'] }}</span></li>
            <li>Date:Time: <span>{{ $Booking['date'] }}</span></li>
            <li>Duration: <span>{{ $Booking['from'] }} / {{ $Booking['to'] }}</span></li>
            <li>Discount:<span>{{ $Booking['dicsount'] }}</span></li>
            <li>Paymet Method: <span>visa</span></li>
        </ul>

        <h4>AMOUNT BILLED</h4>
        <ul>
            <li>Sub Total: <?= round($Booking['total'] / 1.05) ?></li>
            <li>VAT:{{ $Booking['vat'] }}</li>
            <li>Grand Total : {{ $Booking['total'] }}</li>
        </ul>
        <h1 style="text-align: center;color: #ffb938;    font-size: 48px;letter-spacing: .8px;">THANK YOU FOR YOUR
            PAYMENT!</h1>
        <footer>
            <p>Inspiratus Sports District, Dubai Sports City</p>
            <p>04 448 1555 - www.isddubai.com - sports@isddubai.com</p>
        </footer>
    </div>
</body>

</html>
