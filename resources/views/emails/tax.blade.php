<!DOCTYPE HTML PUBLIC "-//W3C//Dtd HTML 4.01//EN" "http://www.w3.org/tr/html4/strict.dtd">
<HTML>

<HEAD>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <TITLE>pdf-html</TITLE>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
    <STYLE type="text/css">
        body {
            margin-top: 0px;
            margin-left: 0px;
            font-family: 'Roboto';
        }

        #page_1 {
            position: relative;
            margin: 0px auto;
            padding: 0px;
            border: none;
            width: 640px
        }

        #page_1 #id1_1 {
            border: none;
            padding: 0px;
            border: none;
            width: 100%;
        }

        #page_1 #id1_2 {
            border: none;
            padding: 0px;
            border: none;
            width: 100%;
        }

        #page_1 #id1_2 #id1_2_1 {
            float: left;
            border: none;
            padding: 0px;
            border: none;
            width: 266px;
            overflow: hidden;
        }

        #page_1 #id1_2 #id1_2_2 {
            float: right;
            border: none;
            margin: 1px 0px 0px 0px;
            padding: 0px;
            border: none;
            width: 50%;
            overflow: hidden;
        }

        #page_1 #id1_3 {
            background: #ebeaf0;
            margin-top: 3px;
            padding: 34px;
            width: 100%;
        }

        #page_1 #p1dimg1 {
            position: absolute;
            top: 0px;
            left: 0px;
            z-index: -1;
            width: 794px;
            height: 1079px;
        }

        #page_1 #p1dimg1 #p1img1 {
            width: 794px;
            height: 1079px;
        }




        .dclr {
            clear: both;
            float: none;
            height: 1px;
            margin: 0px;
            padding: 0px;
            overflow: hidden;
        }

        .ft0 {
            font: bold 33px 'Arial';
            color: #f00d1f;
            line-height: 38px;
        }

        .ft1 {
            font: 16px 'Arial';
            line-height: 18px;
        }

        .ft2 {
            font: 11px 'Arial';
            line-height: 14px;
        }

        .ft3 {
            font: bold 12px 'Arial';
            color: #ffffff;
            line-height: 15px;
        }

        .ft4 {
            font: 12px 'Arial';
            line-height: 15px;
        }

        .ft5 {
            font: 12px 'Arial';
            margin-left: 27px;
            line-height: 15px;
        }

        .ft6 {
            font: 12px 'Arial';
            line-height: 30px;
        }

        .ft7 {
            font: bold 11px 'Arial';
            color: #ffffff;
            line-height: 14px;
        }

        .ft8 {
            font: bold 10px 'Arial';
            color: #ffffff;
            line-height: 12px;
        }

        .ft9 {
            font: 1px 'Arial';
            line-height: 1px;
        }

        .ft10 {
            font: bold 8px 'Arial';
            line-height: 10px;
        }

        .ft11 {
            font: 11px 'Arial';
            line-height: 11px;
        }

        .ft12 {
            font: 8px 'Arial';
            text-decoration: underline;
            line-height: 10px;
        }

        .ft13 {
            font: 8px 'Arial';
            line-height: 10px;
        }

        .ft14 {
            font: 8px 'Arial';
            margin-left: 1px;
            line-height: 10px;
        }

        .ft15 {
            font: bold 9px 'Arial';
            line-height: 11px;
        }

        .ft16 {
            font: 9px 'Arial';
            line-height: 12px;
        }

        .ft17 {
            font: 9px 'Arial';
            line-height: 11px;
        }

        .ft18 {
            font: 12px 'Arial';
            color: #fda914;
            line-height: 15px;
        }

        td {
            line-height: 3;
        }

        .p0 {
            text-align: left;
            margin-top: 0px;

        }

        .p1 {
            text-align: left;
            margin-top: 6px;

        }

        .p2 {
            text-align: left;
            margin-top: 4px;

        }

        .p3 {
            text-align: left;
            padding-left: 9px;
            margin-top: 35px;

        }

        .p4 {
            text-align: left;
            padding-left: 19px;
            margin-top: 29px;

        }

        .p5 {
            text-align: left;
            padding-left: 53px;
            padding-right: 142px;
            margin-top: 23px;

        }

        .p6 {
            text-align: left;
            padding-left: 53px;
            margin-top: 22px;

        }

        .p7 {
            text-align: right;
            margin-top: 0px;

        }

        .p8 {
            text-align: right;
            margin-top: 5px;

        }

        .p9 {
            text-align: right;
            margin-top: 4px;

        }

        .p10 {
            text-align: center;
            margin-top: 0px;

            white-space: nowrap;
        }

        span {
            text-align: center;
            display: block;
            color: #fff;
            line-height: 14px;
        }

        .p11 {
            text-align: center;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p12 {
            text-align: center;
            padding-right: 19px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p13 {
            text-align: center;
            padding-left: 4px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p14 {
            text-align: left;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p15 {
            text-align: center;
            padding-left: 5px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p16 {
            text-align: right;
            padding-right: 33px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p17 {
            text-align: right;
            padding-right: 31px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p18 {
            text-align: right;
            padding-right: 36px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p19 {
            text-align: right;
            padding-right: 88px;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p20 {
            text-align: right;
            margin-top: 0px;

            white-space: nowrap;
        }

        .p21 {
            text-align: justify;
            padding-right: 76px;
            margin-top: 0px;

        }

        .p22 {
            text-align: justify;
            padding-right: 76px;
            margin-top: 5px;

        }

        .p23 {
            text-align: left;
            margin-top: 17px;

        }

        .p24 {
            text-align: left;
            padding-left: 200px;
            margin-top: 62px;

        }

        .p25 {
            text-align: left;
            padding-left: 163px;
            margin-top: 1px;

        }

        .td0 {
            padding: 0px;
            margin: 0px;
            width: 63px;
            vertical-align: bottom;
        }

        .td1 {
            padding: 0px;
            margin: 0px;
            width: 70px;
            vertical-align: bottom;
        }

        .td2 {
            padding: 0px;
            margin: 0px;
            width: 78px;
            vertical-align: bottom;
        }

        .td3 {
            padding: 0px;
            margin: 0px;
            width: 53px;
            vertical-align: bottom;
        }

        .td4 {
            padding: 0px;
            margin: 0px;
            width: 84px;
            vertical-align: bottom;
        }

        .td5 {
            padding: 0px;
            margin: 0px;
            vertical-align: bottom;
        }

        .td6 {
            padding: 0px;
            margin: 0px;
            width: 65px;
            vertical-align: bottom;
        }

        .tr0 {
            height: 18px;
        }

        .tr1 {
            height: 16px;
        }


        .tr3 {
            height: 19px;
        }

        .tr4 {
            height: 23px;
        }

        .tr5 {
            height: 22px;
        }

        .t0 {
            width: 100%;
            margin-top: 80px;
            font: bold 10px 'Arial';
            color: #ffffff;
            border-collapse: separate;
            border-spacing: 0 2px;
        }

        .t1 {
            width: 100%;
            font: 12px 'Arial';
            background: #ebeaf0;
            padding: 21px;
        }

        .tr_bg {
            background: red;
            height: 30px;
            line-height: 30px;
        }

        .tr0 {
            height: 30px;
            border-right: solid 1px #fff;
            vertical-align: top;
            padding-top: 4px;
        }

        .tr0 p {
            font-size: 13px;
            line-height: 15px;
            margin-bottom: 0;
        }

        tr {
            background: #ebeaf0;
            color: #000;
        }

        p.p10.ft4 {
            text-align: left;
            line-height: 3;
        }

        .footer {
            background: #3a2c69;
            text-align: center;
            padding: 10px 0px;
        }

        div#logo {
            float: right;
            margin-bottom: 40px;
        }

    </STYLE>
</HEAD>

<body>
    <div id="page_1">
        <?php
        $users = DB::table('users')
            ->where('id', $details['client_id'])
            ->first();
        $vat = $details['total'] - round($details['total'] / 1.05);
        $subtotal = $details['total'] - $vat;
        ?>

        <div id="id1_2">

            <table style="width:100%">
                <tr style="background: transparent">
                    <td>
                        <P class="p0 ft0">TAX INVOICE</P>
                    </td>
                    <td style="direction: rtl;">
                        <img src="https://bookings.isddubai.com/public//storage/settings/June2021/384dQ9eJeCJTfa6TVUWE.png"
                            alt="" width="150" srcset="">
                    </td>
                </tr>
            </table>
            <div id="id1_2_1">
                <P class="p0 ft1">{{ $users->name }}</P>
                <P class="p1 ft2">Email: {{ $users->email }}</P>
                <P class="p2 ft2">Contact Number: {{ $users->contacts }}</P>
            </div>
            <div id="id1_2_2">
                <P class="p7 ft2">Invoice Date: {{ $details['created_at']->format('d/m/Y') }}</P>
                <P class="p8 ft2">Invoice# <NOBR>FC-{{ $details['code'] }}</NOBR>
                </P>
                <P class="p9 ft2">Payment terms: Immediate</P>
            </div>
            <table cellpadding=0 cellspacing=0 class="t0">
                <tr style="background: red">
                    <td class="tr0 td0">
                        <P class="p10 ft7">S.No</P>
                    </td>
                    <td class="tr0 td0">
                        <P class="p10 ft7">Description</P>
                    </td>
                    <td class="tr0 td0">
                        <P class="p10 ft7">Duration</P>
                        <span>(hrs)</span>
                    </td>
                    <td class="tr0 td1">
                        <P class="p10 ft7">Unit Price</P>
                    </td>
                    <td class="tr0 td2">
                        <P class="p11 ft8">Gross Total</P>
                        <span>AED</span>
                    </td>
                    <td class="tr0 td3">
                        <P class="p10 ft8">VAT %</P>
                    </td>
                    <td class="tr0 td4">
                        <P class="p12 ft8">VAT amount</P>
                    </td>
                    <td class="tr0 td3">
                        <P class="p13 ft7">Net Total</P>
                        <span>AED</span>
                    </td>
                </tr>
                <tr>
                    <td class="tr2 td0">
                        <P class="p10 ">1</P>
                    </td>
                    <td class="tr2 td0">
                        <P class="p10 ft4">sport</P>
                    </td>
                    <td class="tr2 td0">
                        <P class="p10 ">1</P>
                    </td>
                    <td class="tr2 td1">
                        <P class="p16 ft4">{{ $subtotal }}</P>
                    </td>
                    <td class="tr2 td2">
                        <P class="p11 ft2">{{ $subtotal }} AED</P>
                    </td>
                    <td class="tr2 td3">
                        <P class="p17 ft4">5</P>
                    </td>
                    <td class="tr2 td4">
                        <P class="p18 ft4">{{ $vat }}</P>
                    </td>
                    <td class="tr2 td3">
                        <P class="p15 ft2">{{ $details['total'] }}</P>
                    </td>
                </tr>
                <tr>
                    <td class="tr2 td0">
                    </td>
                    <td class="tr2 td0">
                        <P class="p10 ft4">Pitch / Court:</P>
                    </td>
                    <td class="tr2 td0">
                    </td>
                    <td class="tr2 td1">
                        <P class="p16 ft4"></P>
                    </td>
                    <td class="tr2 td2">
                        <P class="p11 ft2"></P>
                    </td>
                    <td class="tr2 td3">

                    </td>
                    <td class="tr2 td4">

                    </td>
                    <td class="tr2 td3">
                        <P class="p15 ft2"></P>
                    </td>
                </tr>
                <tr>
                    <td class="tr2 td0">
                    </td>
                    <td class="tr2 td0">
                        <P class="p10 ft4">Date:</P>
                    </td>
                    <td class="tr2 td0">
                    </td>
                    <td class="tr2 td1">
                        <P class="p16 ft4"></P>
                    </td>
                    <td class="tr2 td2">
                        <P class="p11 ft2"></P>
                    </td>
                    <td class="tr2 td3">

                    </td>
                    <td class="tr2 td4">

                    </td>
                    <td class="tr2 td3">
                        <P class="p15 ft2"></P>
                    </td>
                </tr>
                <tr>
                    <td class="tr2 td0"></td>
                    <td class="tr2 td0">
                        <P class="p10 ft4">Time:</P>
                    </td>
                    <td class="tr2 td0">
                    </td>
                    <td class="tr2 td1">
                        <P class="p16 ft4"></P>
                    </td>
                    <td class="tr2 td2">
                        <P class="p11 ft2"></P>
                    </td>
                    <td class="tr2 td3">

                    </td>
                    <td class="tr2 td4">

                    </td>
                    <td class="tr2 td3">
                        <P class="p15 ft2"></P>
                    </td>
                </tr>
            </table>
            <table cellpadding=0 cellspacing=0 class="t1">
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="tr3 td5">
                        <P class="p19 ft4">Subtotal:</P>
                    </td>
                    <td class="tr3 td6">
                        <P class="p20 ft4">{{ $subtotal }} AED </P>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="tr4 td5">
                        <P class="p19 ft4">5% VAT:</P>
                    </td>
                    <td class="tr4 td6">
                        <P class="p20 ft2">+ {{ $vat }} AED</P>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="tr5 td5">
                        <P class="p19 ft4">Total:</P>
                    </td>
                    <td class="tr5 td6">
                        <P class="p20 ft4">{{ $details['total'] }} AED</P>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="tr4 td5">
                        <P class="p19 ft2">Payment Received:</P>
                    </td>
                    <td class="tr4 td6">
                        <P class="p20 ft4">0.00 AED
                        </P>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="tr4 td5">
                        <P class="p19 ft4">Balance due:</P>
                    </td>
                    <td class="tr4 td6">
                        <P class="p20 ft4">{{ $details['total'] }} AED
                        </P>
                    </td>
                </tr>
            </table>
        </div>
        <div id="id1_3">
            <P class="p21 ft13"><span class="ft10">Terms and conditions: </span><span
                    class="ft11">• </span>Groups are
                capped to 7 aside maximum. <span class="ft11">• </span>Maximum allowed time is <span
                    class="ft12">(60)
                    minutes.</span><span class="ft11"> • </span>Entry and exit will be from the front main
                entrance of
                the football center only. <span class="ft11">• </span>You must wear a mask when entering and
                have your
                temperature checked. • Only 1 single cashless payment will be accepted (no cash/Multiple cards). <span
                    class="ft11">• </span>No bibs or balls will be provided. <span class="ft11">•
                </span>Water will be
                available to purchase as normal. <span class="ft11">• </span>Changing room facilities will
                not be
                available. <span class="ft11">• </span>No spectators allowed. <span class="ft11">•
                </span>Please arrive
                on time and leave directly after the game, no gatherings allowed in the facility. <span
                    class="ft11">•
                </span>If you are late we will not be able to provide you extra time as there is a short gap between
                bookings and there is no option of overlapping groups. <span class="ft11">• </span>The
                Football Center
                and DSC management will not be responsible for your personal belongings and minor/major injuries at the
                facility. <span class="ft11">• </span>In case of any changes/cancellations from The Football
                Center
                senior management or the customer, notice must be given at least <span class="ft12">48 hours
                    priorby
                    email or phone call</span><span class="ft11"> • </span>The payment must be made at the
                front desk
                before your game starts by the organizer. <span class="ft11">• </span>Fees are not refundable
                under any
                cancellation circumstances. <span class="ft11">• </span>Late cancellation fee of 150 AED will
                be
                charged.</P>
            <P class="p22 ft13"><span class="ft10">Liability: </span>The Organizer reserves the right
                not to permit the
                access and remove any Player who behaves improperly or abusively towards him/herself or otherpersons,
                has been involved in a fraudulent activity or otherwise interferes with the normal running of the
                Academy. No refund will be given should the Player be removed from the Academy for these reasons. The
                Organizer shall not be liable to the Player and the Player’s Parents / Guardians for any force majeure
                event or occurrence or any Organizer’saction done based on a legal or regulatory imperative, including
                the suspension or cancellation of the Academy Activities. The Organizer and DSC disclaims any liability
                in relation to Players’ claims and</P>
            <P class="p21 ft13"><span class="ft13">/</span><span class="ft14">or third
                    parties’ claims that may arise as
                    a result of the views and /or statements expressed and / or the use of images and / or personal data
                    that could arise in relation to the Players that may appear in the massmedia. The Football Center
                    and DSC are exempt of any liability in regard to the Academy, which is exclusively the
                    responsibility of the Organizer. I acknowledge the contagious nature of </span>
                <NOBR>COVID-19</NOBR> and understand the risk of becoming exposed to, or infected by, <NOBR>COVID-19
                </NOBR> at the sports facility.On my behalf, and on behalf of my children, I hereby covenant not to sue,
                hold harmless, and release and discharge LaLiga, the Organizer, theFacility and the facility management
                company as well as their successors, employees, agents and representatives, of all liabilities, claims,
                actions,damages, costs or expenses of any kind arising out thereof, or relating thereto, whether a
                <NOBR>COVID-19</NOBR> infection occurs, before, during or afterparticipating in events and activities
                organized by LaLiga or the Organizer.
            </P>
            <P class="p23 ft15">Payment terms:</P>
            <P class="p2 ft16">Upon conﬁrmation</P>
            <P class="p0 ft17">Cash / Check in favor of: Inspiratus Consulting Limited Dubai Br</P>
            <P class="p0 ft17">Bank Details: Emirates NBD, Account Name: Inspiratus Consulting Limited, SWIFT
                Code:
                EBILAEAD, AED Account Number: <NOBR>03-4803</NOBR>
                <NOBR>4845-101,</NOBR> AED IBAN
            </P>
            <P class="p0 ft16">Number: AE370260001014845480303, trN # 100268867700003</P>
        </div>
        <div class="footer">
            <P class=" ft18">Inspiratus Sports District, Dubai Sports City</P>
            <P class=" ft18">04 4481 - www.isddubai.com - sports@isddubai.com</P>
        </div>
    </div>
</body>

</HTML>
