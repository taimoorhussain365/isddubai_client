<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ISD DUBAI - Dashboard</title>
    <!-- Custom fonts for this template-->

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://isddubai.com/assets-web/css/style.css"/>
    <!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script type="text/javascript" src="https://isddubai.com/assets-web/js/functions.js"></script>
    
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <meta name="csrf-token" content="{{ csrf_token() }}" />

  
<style>
.primary-navigation .header-navigation{
    right: 10px !important;
}


.social-links .icon {
    height: 40px;
    width: 40px;
    display: grid;
    align-items: center;
    font-size: 15px;
    justify-content: center;
    text-decoration: none;
    margin: 10px 5px;
    border-radius: 50%;
}

.social-links .icon.fa:hover {
    opacity: 0.7;
}

.social-links .icon.fa-facebook {
    background: #3B5998;
    color: white;
}

.social-links .icon.fa-twitter {
  background: #55ACEE;
  color: white;
}

.fa-google {
  background: #dd4b39;
  color: white;
}

.social-links .icon.fa-linkedin {
  background: #007bb5;
  color: white;
}

.social-links .icon.fa-youtube {
  background: #bb0000;
  color: white;
}

.social-links .icon.fa-instagram {
  background: #E1306C;
  color: white;
}

.social-links .icon.fa-pinterest {
  background: #cb2027;
  color: white;
}

.social-links .icon.fa-snapchat-ghost {
  background: #fffc00;
  color: white;
  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}

.social-links .icon.fa-skype {
  background: #00aff0;
  color: white;
}

.social-links .icon.fa-android {
  background: #a4c639;
  color: white;
}

.social-links .icon.fa-dribbble {
  background: #ea4c89;
  color: white;
}

.social-links .icon.fa-vimeo {
  background: #45bbff;
  color: white;
}

.social-links .icon.fa-tumblr {
  background: #2c4762;
  color: white;
}

.social-links .icon.fa-vine {
  background: #00b489;
  color: white;
}

.social-links .icon.fa-foursquare {
  background: #45bbff;
  color: white;
}

.social-links .icon.fa-stumbleupon {
  background: #eb4924;
  color: white;
}


</style>

</head>

<body class="bg-init innerpage">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    

    <main class="app-container">
        @include('includes.navigation')


        {{-- session message --}}
        @if(Session::has('success') || Session::has('error') || Session::has('info'))
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <strong>Success!</strong> {{ Session::get('success') }}
                    </div>
                    @elseif(Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <strong>Error!</strong> {{ Session::get('error') }}
                    </div>
                    @elseif(Session::has('info'))
                    <div class="alert alert-info" role="alert">
                        <strong>Info!</strong> {{ Session::get('info') }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
        @endif
        {{--  End of message --}}

        <div class="container-wrapper ">
          <div class="row">
            <div class="col-md-12 text-right sidebar-navigation">

              <h2> Wallet : AED {{ isset($walletTotalAmnt->total) && $walletTotalAmnt->total ? $walletTotalAmnt->total : '0' }}<h2>

            </div>
          </div>
        </div>

        
        @yield('content')

        @include('includes.footer')
    </main>
    
    <script type="text/javascript" src="https://isddubai.com/assets-web/js/script.js"></script>

   {{--  <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('public/assets-user/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('public/assets-user/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{ asset('public/assets-user/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{ asset('public/assets-user/js/sb-admin-2.min.js')}}"></script>
    <!-- Page level plugins -->
    <script src="{{ asset('public/assets-user/vendor/chart.js/Chart.min.js')}}"></script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('public/assets-user/js/demo/chart-area-demo.js')}}"></script>
    <script src="{{ asset('public/assets-user/js/demo/chart-pie-demo.js')}}"></script>
    <!-- Page level plugins -->
    <script src="{{ asset('public/assets-user/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('public/assets-user/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Page level custom scripts -->
    <script src="{{ asset('public/assets-user/js/demo/datatables-demo.js')}}"></script> --}}

    
</body>

</html>