<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Payment extends Model
{
    
    protected  $table = "payment";

    public function getClientIdAttribute($v)
    {
        $name = User::where('id',$v)->value('name');
        return $name;
    }

    public function getBookIdAttribute($v)
    {
        $name = Booking::where('id',$v)->value('code');
        return $name;
    }

}
