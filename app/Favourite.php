<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Favourite extends Model
{
    protected $appends = ['sportName'];

    public function court()
    {
        return $this->belongsTo('App\Court');
    }
    
    public function getSportNameAttribute($v)
    {
        $name = Sport::where('id',$this->sport_id)->value('name');
         return $name;
    }
}
